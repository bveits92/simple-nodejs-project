const http = require('http');
const fs = require('fs');
const path = require('path'); // Import the path module

const index = http.createServer((req, res) => {
    console.log(req.url, req.method);

    // Set header content type
    res.setHeader('Content-Type', 'text/html');

    // Construct the file path using path.join
    let filePath = path.join('./views', req.url === '/' ? 'index.html' : req.url + '.html');

    // Send an HTML file
    fs.readFile(filePath, (err, data) => {
        if (err) {
            console.log(err);
            res.writeHead(404);
            res.end('File not found');
        } else {
            res.end(data);
        }
    });
});

index.listen(3000, 'localhost', () => {
    console.log('Listening for requests on port 3000');
});
